# Git Commands

1. `git init`- Initialized a git repository
2. `git status`- Displays the status of the files in the local repository
3. `git add` - Adds the files in the working directory to the staging area
4. `git commit` - Commits the changes to the local repository
5. `git log` - Outputs the log of commits in the descending order of the commit chain
