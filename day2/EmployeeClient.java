package day2;

public class EmployeeClient {
    
    public static void main(String[] args) {
        PermanentEmployee ramesh = new PermanentEmployee( "Ramesh", "ramesh@gmail.com", 2_50_000);
        ContractEmployee suresh = new ContractEmployee("Suresh","suresh@gmail.com", 60, 3000);
        suresh.setEmailAddress("suresh@gmail.com");

        Dependent vinay = new Dependent("Vinay", 12, Relationship.SON, Gender.MALE);
        Dependent vaishali = new Dependent("Vaishali", 8, Relationship.DAUGHTER, Gender.FEMALE);

        suresh.setPayPerHour(3000);
        suresh.setDuration(60);

        ramesh.addDependent(vinay);
        ramesh.addDependent(vaishali);

        Employee emp1 = ramesh;
        Employee emp2 = suresh;
        

        /* ramesh.setName("Ramesh");
        ramesh.setEmailAddress("ramesh@gmail.com");
        ramesh.setEmpId(2222);
        ramesh.setSalary(2_50_000); */


        System.out.println("Employee ID " + ramesh.getEmpId() );
        System.out.println("Employee ID " + suresh.getEmpId() );
/*         System.out.println("Employee name " + ramesh.getName() );
        System.out.println("Employee email address " + ramesh.getEmailAddress());
        System.out.println("Employee Salary " + ramesh.getSalary());
 */
     /*    ramesh.getDependents()
            .forEach((dep) -> System.out.println(dep)); */


        double tdsForRamesh = emp1.calculateTDS();
        double tdsForSuresh = emp2.calculateTDS();

        System.out.println("TDS for Ramesh :: "+tdsForRamesh);
        System.out.println("TDS for Suresh :: "+tdsForSuresh);

        //apply for leaves
        try {
            ramesh.applyForLeave(10);
            System.out.println("Balance leave for Ramesh "+ ramesh.getLeaves());
            ramesh.applyForLeave(12);
            System.out.println("Balance leave for Ramesh "+ ramesh.getLeaves());
        } catch(InsufficientLeaveBalanceException exception){
            System.out.println("You do not have suffiencient leave balance to apply for leave ");
            System.out.println("Available leave balance :: "+ ramesh.getLeaves());
        } 
    }
}
